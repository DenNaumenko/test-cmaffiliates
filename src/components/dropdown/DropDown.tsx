"use client"

import * as React from 'react';
import { Theme, useTheme } from '@mui/material/styles';
import OutlinedInput from '@mui/material/OutlinedInput';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { Dispatch, SetStateAction } from 'react';
import { TInputValues } from '@/types';

interface IDropDownProps {
  list: string[],
  cityName: string[],
  setInputValue: Dispatch<SetStateAction<TInputValues>>
}

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
      backgroundColor: "#313131",
      color: "#fff"
    },
  },
};

function getStyles(name: string, cityName: readonly string[], theme: Theme) {
  return {
    fontWeight:
      cityName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

export default function DropDown({ list, cityName, setInputValue }: IDropDownProps) {
  const theme = useTheme();

  const handleChange = (event: SelectChangeEvent<typeof cityName>) => {
    const {
      target: { value },
    } = event;
    setInputValue((prevState: TInputValues) => ({ ...prevState, cityName: typeof value === 'string' ? value.split(',') : value}));
  };

  return (
        <FormControl sx={{
            m: 0,
            width: 'auto',
          }}>
          <Select
            multiple
            displayEmpty
            value={cityName}
            onChange={handleChange}
            input={<OutlinedInput />}
            renderValue={(selected) => {
              if (selected.length === 0) {
                return <span>Country</span>;
              }
              return selected.join(', ');
            }}
            MenuProps={MenuProps}
            inputProps={{ 'aria-label': 'Without label' }}
            sx={{
              color: "#A4A4A4",
              borderRadius: '0.75rem',
              width: "10rem",
              height: "2.5rem",
              padding: "0.6875rem 1rem",
              border: "1px solid #515151",
              background: "#313131",
              fontSize: "0.875rem",
              fontFamily: "Helvetica Now Display",
              fontStyle: "normal",
              fontWeight: "500",
              lineHeight: "1.125rem",
              letterSpacing: "0.00875rem",
              ".MuiSelect-select": {
                padding: 0,
              },
              ".MuiList-root.MuiMenu-list": {
                color: "#fff",
                backgroundColor: "#313131",
              }
            }}
          >
            <MenuItem disabled value="">
              <span>Country</span>
            </MenuItem>
            {list.map((name: string) => (
              <MenuItem
                key={name}
                value={name}
                style={getStyles(name, cityName, theme)}
                sx={{
                  backgroundColor: "#313131",
                  color: "#A4A4A4",
                  padding: "0.6875rem 1rem",
                  "&:hover": {
                    backgroundColor: "#191919",
                    cursor: "pointer",
                  },
                  "&.Mui-selected": {
                    backgroundColor: "#191919",
                  },
                  "&.Mui-selected:hover": {
                    backgroundColor: "#191919",
                    opacity: "0.8"
                  },
                  "&.Mui-focusVisible": {
                    backgroundColor: "#191919",
                    opacity: "0.8"
                  },
                  "&.Mui-selected.Mui-focusVisible": {
                    backgroundColor: "#191919",
                    opacity: "0.8"
                  }
                }}
              >
                {name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
  );
}