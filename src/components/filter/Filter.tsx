"use client"

import DropDown from "@/components/dropdown/DropDown";
import Input from "@/components/input/Input";
import styles from './Filter.module.css'
import {cityes} from '@/API/cityes';
import { TInputValues } from "@/types";
import { Dispatch, SetStateAction } from "react";

interface IFilterProps {
    inputValue: TInputValues,
    setInputValue: Dispatch<SetStateAction<TInputValues>>
}

const Filter = ({ inputValue, setInputValue }: IFilterProps) => {

    const handleChange = (name: string, value: string) => {
        setInputValue((prevState: TInputValues) => ({ ...prevState, [name]: value }));
    };

    return (
        <div className={styles.filters}>
            <DropDown list={cityes} cityName={inputValue.cityName} setInputValue={setInputValue} />

            <Input
            placeholder="min"
            name="min"
            value={inputValue.value}
            onChange={handleChange}
            max={inputValue.max || '80'}/>

            <Input
            placeholder="max"
            name="max"
            value={inputValue.value}
            onChange={handleChange}
            min={inputValue.min || '-80'}
            />
        </div>
    );
}

export default Filter;