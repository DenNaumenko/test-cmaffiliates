'use client'
import { useState, ChangeEvent } from 'react';
import styles from './Input.module.css'

interface IInputProps {
    placeholder:string,
    onChange:(name: string, value: string) => void,
    name:string,
    value?:string,
    min?:string,
    max?:string
}

const Input = ({ placeholder, onChange, name, value, min = '-80', max = '80' }:IInputProps) => {
    const [val, setVal] = useState(value);

    const setNumber = (e: ChangeEvent<HTMLInputElement>) => {
        let { value, min, max, name } = e.target;

        if (value !== '') {
            value = `${Math.floor(Math.max(Number(min), Math.min(Number(max), Number(value))))}`;
        }

        setVal(value);
        onChange(name, value);
    };

    return (
        <input
        className={styles.input}
        type="number"
        min={min}
        max={max}
        name={name}
        value={val}
        onChange={setNumber}
        placeholder={placeholder}
        />
    );
};

export default Input;