import React, { useEffect, useRef } from "react";
import Highcharts from "highcharts";
import styles from "./Chart.module.css";
import { TActiveCityValues } from "@/types";


interface IChartProps {
  activeCity: TActiveCityValues,
}

const Chart = ({ activeCity: {name, daily: { temperature_2m_max, temperature_2m_min, time }} }:IChartProps) => {
  const chartRef = useRef(null);

  useEffect(() => {
    const arrayOfTemp = temperature_2m_max.map((value, index) => {
      return Math.floor((value + temperature_2m_min[index])/2);
    });
    const daysOfWeek = [
      "Mon",
      "Tue",
      "Wed",
      "Thu",
      "Fri",
      "Sat",
      "Sun"
    ];
    const dayOfWeek = new Date(time[0]).getDay();
    const newArray = daysOfWeek
      .slice(dayOfWeek)
      .concat(daysOfWeek.slice(0, dayOfWeek));

    if (chartRef.current) {
      Highcharts.chart(chartRef.current, {
        chart: {
          type: "column",
          backgroundColor: "#191919",
          spacing: [16, 24, 12, 24],
          width: 453
        },
        title: {
          text: "Analytics",
          align: "left",
          style: {
            color: "#fff",
            fontSize: "1rem",
            fontFamily: "Helvetica Now Display",
            fontStyle: "normal",
            fontWeight: 700,
            lineHeight: "1.5rem",
            letterSpacing: "0.01rem"
          },
          margin: 19,
        },
        xAxis: {
          tickWidth: 0,
          labels: {
            style: {
              color: "#FDFCFF",
              fontSize: "0.875rem",
              fontFamily: "Helvetica Now Display",
              fontStyle: "normal",
              fontWeight: 400,
              lineHeight: "1.125rem",
              letterSpacing: "0.00875rem"
            }
          },
          categories: newArray
        },
        yAxis: {
          startOnTick: false,
          gridLineWidth: 0.5,
          gridLineDashStyle: "longdash",
          gridLineColor: "#A4A4A4",
          gridLineDashLength: 50,
          max: 80,
          title: {
            text: "",
            style: {
              color: "#A4A4A4",
              fontSize: "0.875rem",
              fontFamily: "Helvetica Now Display",
              fontStyle: "normal",
              fontWeight: 500,
              lineHeight: "1.125rem",
              letterSpacing: "0.00875rem"
            }
          },
          labels: {
            formatter: function (): string {
              return Highcharts.numberFormat(this.value, 0, "", ",") + "°C";
            },
            style: {
              color: "#A4A4A4"
            }
          } as any
        },
        legend: {
          enabled: false
        },
        credits: {
          enabled: false
        },
        tooltip: {
          valueSuffix: "°C"
        },
        plotOptions: {
          column: {
            borderRadius: 8,
            pointPadding: 0,
            groupPadding: 0.2,
            borderWidth: 0,
            threshold: -80,
            color: {
              linearGradient: {
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 1
              },
              stops: [
                [0, "#B3FC4F"],
                [1, "#173102"]
              ]
            }
          }
        },
        series: [
          {
            name: "Temperature",
            data: arrayOfTemp
          }
        ]
      } as any);
    }
  }, [name]);

  return (
    <div className={styles.chart}>
      <div ref={chartRef} />
      <span className={styles.val_name}>{name}</span>
    </div>
  );
};

export default Chart;