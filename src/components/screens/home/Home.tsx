"use client"

import { FC, useState, ChangeEvent, useEffect, SetStateAction } from "react";
import styles from './Home.module.css'
import Chart from "@/components/chart/Chart";
import {cityes} from '@/API/cityes';
import OMapi from "@/API/OMapi";
import Spinner from "@/components/spinner/Spinner";
import Filter from "@/components/filter/Filter";
import { filterCityList } from "@/utils/filterCityList";
import { TCityesValues, TInputValues } from "@/types";


const Home = () => {
    const [allCityes, setCityes] = useState<TCityesValues[]>([]);
    const [activeCity, setActiveCity] = useState<any>({});
    const [loading, setLoading] = useState(true);
    const [inputValue, setInputValue] = useState<TInputValues>({
        cityName: [],
        max: '',
        min: ''
    });
    const api = new OMapi;

    useEffect(() => {
        let requests = cityes.map(city => api.getCity(city));
        Promise.all(requests).then((values: TCityesValues[]) => {
            setCityes(values);
            setActiveCity(values[0]);
            setLoading(false);
        });
    }, []);

    const renderHeaderList = () => {
        return (
          <div className={styles.list_header}>
            <div className={styles.content_left}>City</div>
            <div>Temparature max</div>
            <div>Temparature min</div>
            <div>Wind direction</div>
          </div>
        );
    };

    const renderCityesList = () => {
        const filteredList = filterCityList(allCityes, inputValue);
        const renderList = filteredList.length ? filteredList : allCityes;
        const list =  renderList.map((city: TCityesValues, i: number) => {
            return (<div className={styles.list_record} key={i} onClick={() => setActiveCity(city)}>
                <div className={styles.content_left}>{city.name}</div>
                <div>{city.todayMaxT}</div>
                <div>{city.todayMinT}</div>
                <div>{city.windDirection}</div>
            </div>);
        });

        return (
            <div className={styles.list_body}>
                {
                    filteredList.length ? list : (
                        <div className={styles.list_record}>
                           <div className={styles.nothing_found}> Nothing found </div>
                        </div>)
                }
            </div>
        );
    };

    if (loading) {
        return <Spinner />;
    }

    return (
        <div className={styles.view}>
            <div className={styles.left_block}>
                { allCityes.length ? <Chart
                    activeCity={activeCity}/>
                : null }
            </div>
            <div className={styles.right_block}>
                <Filter inputValue={inputValue} setInputValue={setInputValue}/>
                <div className={styles.table}>
                    {renderHeaderList()}
                    {renderCityesList()}
                </div>
            </div>
        </div>
    );
}

export default Home;