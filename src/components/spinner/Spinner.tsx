import styles from './Spinner.module.css';

const Spinner = () => {
  return (
    <div className={styles.lds_css}>
      <div className={styles.lds_double_ring}>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export default Spinner;
