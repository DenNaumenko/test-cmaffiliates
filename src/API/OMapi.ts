import { TActiveCityValues, TApiCityValues } from "@/types";

export default class OMapi{

    _apiBase = 'https://geocoding-api.open-meteo.com/v1/search?name=';
    _cityPath = (latitude: string, longitude: string) => `https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&hourly=temperature_2m,winddirection_10m&daily=temperature_2m_max,temperature_2m_min,winddirection_10m_dominant&timezone=auto`;

    getResource = async (url: string) => {
      const res = await fetch(`${this._apiBase}${url}`);

      if (!res.ok) {
        throw new Error(`Could not fetch ${url}` +
          `, received ${res.status}`)
      }
      return await res.json();
    };

    getCity = async (name: string) => {
        return await this.getResource(name)
        .then( data => {
           const result = data.results.filter((el: TApiCityValues) => el.name === name);
            if (!result) {
                throw new Error(`Could not fetch, received ${name}`);
            }

           return result[0];
        })
        .then( async data => {
            const res = await fetch(`${this._cityPath(data.latitude, data.longitude)}`);

            return await res.json();
        })
        .then(data => {
            return this._transformData(data, name);
        });
    }

    _transformData = (data: TActiveCityValues, name: string) => {
        return {
            name: name,
            daily: data.daily,
            todayMaxT: data.daily.temperature_2m_max[0],
            todayMinT: data.daily.temperature_2m_min[0],
            windDirection: data.daily.winddirection_10m_dominant[0]
        };
    }
  }
