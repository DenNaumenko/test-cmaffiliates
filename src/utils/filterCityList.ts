import { TInputValues, TCityesValues } from "@/types";

export const filterCityList = (cityes: TCityesValues[], selectedCityes: TInputValues) => {
    return cityes.filter((item) => {
        if (selectedCityes.cityName.includes(item.name) || !selectedCityes.cityName.length ) {
            if (+selectedCityes.min <= item.todayMinT || !selectedCityes.min) {
                if (+selectedCityes.max >= item.todayMaxT || !selectedCityes.max) {
                    return item;
                }
            }
        }
    });
};

