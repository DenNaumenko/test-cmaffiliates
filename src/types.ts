export type TInputValues = {
    cityName: string[],
    value?: string,
    max: string,
    min: string
};
export type TCityesValues = {
    name: string,
    daily: TDailyValues,
    todayMaxT: number,
    todayMinT: number,
    windDirection: number
};

export type TActiveCityValues = {
    name?: string,
    daily: TDailyValues,
};

export type TDailyValues = {
    temperature_2m_max: number[],
    temperature_2m_min: number[],
    time: number[],
    winddirection_10m_dominant: number[]
};

export type TApiCityValues = {
    name: string
};
